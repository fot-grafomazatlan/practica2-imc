package com.example.practica2imc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView txtvAltura;
    private EditText etxtAltura;
    private TextView txtvPeso;
    private EditText etxtPeso;
    private TextView txtvIMC;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnFinalizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtvAltura = (TextView) findViewById(R.id.txtvAltura);
        etxtAltura = (EditText) findViewById(R.id.etxtAltura);
        txtvPeso = (TextView) findViewById(R.id.txtvPeso);
        etxtPeso = (EditText) findViewById(R.id.etxtPeso);
        txtvIMC = (TextView) findViewById(R.id.txtvIMC);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnFinalizar = (Button) findViewById(R.id.btnFinalizar);

        btnCalcular.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (etxtAltura.getText().toString().matches("") ||
                etxtPeso.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this, "Ingresa los datos correctos", Toast.LENGTH_SHORT).show();
                }else{
                    Double alt = Double.parseDouble(etxtAltura.getText().toString());
                    Double pes = Double.parseDouble(etxtPeso.getText().toString());
                    Double res = pes / (alt*alt);
                    txtvIMC.setText(res.toString());

                }
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etxtAltura.setText("");
                etxtPeso.setText("");
                txtvIMC.setText("");
            }
        });
        btnFinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }
}
